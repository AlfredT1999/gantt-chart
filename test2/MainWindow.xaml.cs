﻿using Business.Activity;
using Business.DateHandler;
using DataAccess.DTOs;
using DataAccess.Models;
using Infragistics.Documents.Excel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;

namespace test2
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // HANDLE COLORS:
            List<Color> colorList = new()
            {
                Color.LightBlue,
                Color.LightGreen,
                Color.LightGray,
                Color.LightYellow,
                Color.LightPink,
                Color.Yellow,
                Color.Red,
                Color.Blue,
                Color.Purple
            };
            List<ActivityDto> activityList = ActivityHandler.GetAllActivities();
            List<TblAuditor> auditorsList = ActivityHandler.GetAllAuditors();

            // SET HEADERS PARAMETERS FOR EXCEL: 
            Workbook Gantt = new();
            Worksheet worksheet1 = Gantt.Worksheets.Add("Gantt_Chart");
            IWorkbookFont normalFont = Gantt.Styles.NormalStyle.StyleFormat.Font;

            // Specifying the name of default font
            normalFont.Name = "Calibri";
            // Specifying the font size. Font size height is measured in twips. One twips is 1/20th of a point
            normalFont.Height = 9 * 20;

            worksheet1.MergedCellsRegions.Add(0, 0, 1, 0);
            worksheet1.Columns[0].Width = 1200;
            worksheet1.Rows[0].Cells[0].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[0].Cells[0].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[0].Cells[0].CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
            worksheet1.Rows[0].Cells[0].Value = "NUM";

            // Border cell = 
            worksheet1.Rows[0].Cells[0].CellFormat.TopBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[0].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[0].Cells[0].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[0].Cells[0].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[0].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[0].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;

            worksheet1.MergedCellsRegions.Add(0, 1, 1, 1);
            worksheet1.Columns[1].Width = 5000;
            worksheet1.Rows[0].Cells[1].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[0].Cells[1].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[0].Cells[1].CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
            worksheet1.Rows[0].Cells[1].Value = "ACTIVIDAD";

            // Border cell = 
            worksheet1.Rows[0].Cells[1].CellFormat.TopBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[1].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[0].Cells[1].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[0].Cells[1].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[1].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[1].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;

            worksheet1.MergedCellsRegions.Add(0, 2, 1, 2);
            worksheet1.Columns[2].Width = 3000;
            worksheet1.Rows[0].Cells[2].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[0].Cells[2].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[0].Cells[2].CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
            worksheet1.Rows[0].Cells[2].Value = "AUD.";

            // Border cell = 
            worksheet1.Rows[0].Cells[2].CellFormat.TopBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[2].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[0].Cells[2].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[0].Cells[2].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[2].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[2].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;

            worksheet1.MergedCellsRegions.Add(0, 3, 1, 3);
            worksheet1.Columns[3].Width = 1000;
            worksheet1.Rows[0].Cells[3].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[0].Cells[3].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[0].Cells[3].CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
            worksheet1.Rows[0].Cells[3].Value = "T";

            // Border cell = 
            worksheet1.Rows[0].Cells[3].CellFormat.TopBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[3].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[0].Cells[3].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[0].Cells[3].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[3].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[3].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;

            int firstRow = 2;
            int lastRow = 3;
            string auditores = "";

            // First part of the excel:
            for (int j = 2, x = 0, y = 2; j <= activityList.Count + 1; j++, x++, y += 2)
            {
                worksheet1.MergedCellsRegions.Add(firstRow, 0, lastRow, 0);
                worksheet1.Rows[y].Cells[0].Value = x + 1;
                worksheet1.Rows[y].Cells[0].CellFormat.Alignment = HorizontalCellAlignment.Center;
                worksheet1.Rows[y].Cells[0].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;

                // Border cell = 
                worksheet1.Rows[y + 1].Cells[0].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y].Cells[0].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y].Cells[0].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y + 1].Cells[0].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y + 1].Cells[0].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;

                worksheet1.MergedCellsRegions.Add(firstRow, 1, lastRow, 1);
                worksheet1.Columns[1].Width = 5000;
                worksheet1.Rows[y].Cells[1].Value = activityList[x].DescripcionActividad;
                worksheet1.Rows[y].Cells[1].CellFormat.Alignment = HorizontalCellAlignment.Center;
                worksheet1.Rows[y].Cells[1].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                worksheet1.Rows[y].Cells[1].CellFormat.WrapText = ExcelDefaultableBoolean.True;

                // Border cell = 
                worksheet1.Rows[y + 1].Cells[1].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y].Cells[1].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y].Cells[1].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y + 1].Cells[1].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y + 1].Cells[1].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;

                // Make autitores strings:
                for (int i = 0; i< activityList[x].Auditores?.Count; i++)
                {
#pragma warning disable CS8602 // Dereference of a possibly null reference.
                    auditores += activityList[x].Auditores[i]?.Abreviacion;
#pragma warning restore CS8602 // Dereference of a possibly null reference.
                    auditores += " ";
                }

                worksheet1.MergedCellsRegions.Add(firstRow, 2, lastRow, 2);
                worksheet1.Rows[y].Cells[2].Value = auditores;
                worksheet1.Rows[y].Cells[2].CellFormat.Alignment = HorizontalCellAlignment.Center;
                worksheet1.Rows[y].Cells[2].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                worksheet1.Rows[y].Cells[2].CellFormat.WrapText = ExcelDefaultableBoolean.True;

                // Border cell = 
                worksheet1.Rows[y + 1].Cells[2].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y].Cells[2].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y].Cells[2].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y + 1].Cells[2].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y + 1].Cells[2].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
                auditores = "";

                worksheet1.Rows[y].Cells[3].CellFormat.Fill = new CellFillPattern(new WorkbookColorInfo(Color.Orange), null, FillPatternStyle.Solid);
                worksheet1.Rows[y].Cells[3].Value = "E";
                worksheet1.Rows[y].Cells[3].CellFormat.Alignment = HorizontalCellAlignment.Center;
                worksheet1.Rows[y].Cells[3].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                worksheet1.Rows[y].Cells[3].CellFormat.WrapText = ExcelDefaultableBoolean.True;

                // Border cell = 
                worksheet1.Rows[y].Cells[3].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y].Cells[3].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y].Cells[3].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y + 1].Cells[3].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y + 1].Cells[3].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;

                worksheet1.Rows[y + 1].Cells[3].Value = "R";
                worksheet1.Rows[y + 1].Cells[3].CellFormat.Alignment = HorizontalCellAlignment.Center;
                worksheet1.Rows[y + 1].Cells[3].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                worksheet1.Rows[y + 1].Cells[3].CellFormat.WrapText = ExcelDefaultableBoolean.True;

                // Border cell = 
                worksheet1.Rows[y + 1].Cells[3].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y + 1].Cells[3].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[y + 1].Cells[3].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;

                firstRow += 2;
                lastRow += 2;
            }

            // JSON Dates:
            List<DateDto> dates = new();
            List<Date> dateList = DateHandler.GetMonthsWithDays();
            List<int> days = new();
            List<int> weekendDays = new();
            List<int> dayOffs = new();
            int cellsCount = 3;
            int cellsStart = 4;
            
            dates.Add(new DateDto
            {
                NumberMonth = 1,
                Month = "Enero",
                Days = new List<int> { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31 }
            });

            dates.Add(new DateDto
            {
                NumberMonth = 2,
                Month = "Febrero",
                Days = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28 }
            });

            dates.Add(new DateDto
            {
                NumberMonth = 3,
                Month = "Marzo",
                Days = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 }
            });

            dates.Add(new DateDto
            {
                NumberMonth = 4,
                Month = "Abril",
                Days = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 }
            });

            dates.Add(new DateDto
            {
                NumberMonth = 5,
                Month = "Mayo",
                Days = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 }
            });

            dates.Add(new DateDto
            {
                NumberMonth = 6,
                Month = "Junio",
                Days = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 }
            });

            dates.Add(new DateDto
            {
                NumberMonth = 7,
                Month = "Julio",
                Days = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 }
            });

            dates.Add(new DateDto
            {
                NumberMonth = 8,
                Month = "Agosto",
                Days = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 }
            });

            dates.Add(new DateDto
            {
                NumberMonth = 9,
                Month = "Septiembre",
                Days = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 }
            });

            dates.Add(new DateDto
            {
                NumberMonth = 10,
                Month = "Octubre",
                Days = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 }
            });

            dates.Add(new DateDto
            {
                NumberMonth = 11,
                Month = "Noviembre",
                Days = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 }
            });

            dates.Add(new DateDto
            {
                NumberMonth = 12,
                Month = "Diciembre",
                Days = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 }
            });

            // Second part of the excel:
            for (int i = 1, x = 4, y = 0; i <= dates.Count; i++, y++)
            {
                worksheet1.Rows[0].Cells[x].CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
                worksheet1.Rows[0].Cells[x].CellFormat.Fill = new CellFillPattern(new WorkbookColorInfo(colorList[y]), null, FillPatternStyle.Solid);
                worksheet1.Rows[0].Cells[x].Value = dates[y].Month;// Current month

                days = DateHandler.TotalDays(dates[y]);
                weekendDays = DateHandler.GetWeekendDays(dates[y]);
                dayOffs = DateHandler.GetDayOffs(dates[y]);

                for (int j = cellsStart, z = 0; z < days.Count; j++, z++)
                {
                    // Make some column headers:
                    worksheet1.Columns[j].Width = 1000;
                    worksheet1.Rows[1].Cells[j].Value = days[z];    
                    cellsCount++;

                    // Check if its weekend day:
                    for (int index = 0; index < weekendDays.Count; index++)
                    {
                        if (days[z] == weekendDays[index])
                        {
                            int columnWeekend = worksheet1.Rows[2].Cells[j].ColumnIndex;
                            
                            for (int index2 = 2; index2 < firstRow; index2++)
                            {
                                worksheet1.Rows[index2].Cells[columnWeekend].CellFormat.Fill = new CellFillPattern(new WorkbookColorInfo(colorList[5]), null, FillPatternStyle.Solid);
                            }
                        }
                    }

                    // Check if its day off:
                    for (int index = 0; index < dayOffs.Count; index++)
                    {
                        if (days[z] == dayOffs[index])
                        {
                            int columnDayOff = worksheet1.Rows[2].Cells[j].ColumnIndex;

                            for (int index2 = 2; index2 < firstRow; index2++)
                            {
                                worksheet1.Rows[index2].Cells[columnDayOff].CellFormat.Fill = new CellFillPattern(new WorkbookColorInfo(colorList[8]), null, FillPatternStyle.Solid);
                            }
                        }
                    }
                }
                
                WorksheetMergedCellsRegion mergedRegion1 = worksheet1.MergedCellsRegions.Add(0, x, 0, cellsCount);

                // Set the value of the merged region (The name of the month):
                mergedRegion1.Value = dateList[y].Month;

                // Border cell = 
                worksheet1.Rows[0].Cells[x].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
                worksheet1.Rows[0].Cells[x].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;

                // Set the cell alignment of the middle cell in the merged region.
                worksheet1.Rows[0].Cells[cellsCount].CellFormat.Alignment = HorizontalCellAlignment.Center;

                x = cellsCount + 1;
                cellsStart = cellsCount + 1;
            }

            // Border cell = 
            for (int i = 1; i < lastRow - 1; i++)
            {
                for (int j = 4; j < cellsCount + 2; j++)
                {
                    worksheet1.Rows[i].Cells[j].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
                    worksheet1.Rows[i].Cells[j].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
                    worksheet1.Rows[i].Cells[j].CellFormat.TopBorderStyle = CellBorderLineStyle.Thin;
                    worksheet1.Rows[i].Cells[j].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
                }
            }

            // JSON for handling tasks:
            List <TaskDto> tasks = new()
            {
                new TaskDto
                {
                    IdActividad = 1,
                    InitialDate = new DateTime(2022, 1, 28),
                    FinalDate = new DateTime(2022, 2, 1),
                    ActualDate = new DateTime(2022, 2, 3),
                    Deviation = 0,
                    Remainder = 0
                },
                new TaskDto
                {
                    IdActividad = 2,
                    InitialDate = new DateTime(2022, 2, 1),
                    FinalDate = new DateTime(2022, 2, 9),
                    ActualDate = new DateTime(2022, 2, 9),
                    Deviation = 0,
                    Remainder = 0
                },
                new TaskDto
                {
                    IdActividad = 3,
                    InitialDate = new DateTime(2022, 3, 4),
                    FinalDate = new DateTime(2022, 3, 7),
                    ActualDate = new DateTime(2022, 3, 4),
                    Deviation = 0,
                    Remainder = 0
                },
                new TaskDto
                {
                    IdActividad = 4,
                    InitialDate = new DateTime(2022, 2, 7),
                    FinalDate = new DateTime(2022, 2, 11),
                    ActualDate = new DateTime(2022, 2, 10),
                    Deviation = 0,
                    Remainder = 0
                },
                new TaskDto
                {
                    IdActividad = 5,
                    InitialDate = new DateTime(2022, 1, 31),
                    FinalDate = new DateTime(2022, 2, 11),
                    ActualDate = new DateTime(2022, 2, 11),
                    Deviation = 0,
                    Remainder = 0
                }
            };
            List<DateRange> estimate = new();
            List<DateRange> real = new();
            int cellStartPos = 4;
            int rowStartPos = 2;
            int sumEstimado = 0;
            int sumReal = 0;
            // TimeSpan dateSubstraction = new(); 

            tasks = tasks.OrderBy(o => o.IdActividad).ToList();// sort tasks

            // Third part of the excel:
            for (int i = 4, x = 0; i < cellsCount; i++, x++)// rows
            {
                if (tasks.Count == x) break;

                // dateSubstraction = tasks[x].InitialDate.Subtract(tasks[x].FinalDate);
                estimate = DateHandler.GetDateRangeEstimate(tasks[x]);
                real = DateHandler.GetDateRangeReal(tasks[x]);
                sumEstimado += estimate.Count;
                sumReal += real.Count;

                for (int j = 2; j <= (cellsStart - 3); j++)// column
                {
                    var test1 = worksheet1.Rows[0].Cells[cellStartPos].AssociatedMergedCellsRegion;//month
                    var test2 = worksheet1.Rows[1].Cells[cellStartPos].Value;//days

                    for(int k = 0; k < estimate.Count; k++)
                    {
                        if (estimate[k].Month?.ToString() == test1.Value.ToString() && 
                            estimate[k].Day.ToString() == test2.ToString())
                        {
                            worksheet1.Rows[rowStartPos].Cells[cellStartPos].CellFormat.Fill = new CellFillPattern(
                                new WorkbookColorInfo(colorList[6]), 
                                null, 
                                FillPatternStyle.Solid);

                            break;
                        }
                    }

                    for (int k = 0; k < real.Count; k++)
                    {
                        if (real[k].Month?.ToString() == test1.Value.ToString() &&
                            real[k].Day.ToString() == test2.ToString())
                        {
                            worksheet1.Rows[rowStartPos + 1].Cells[cellStartPos].CellFormat.Fill = new CellFillPattern(
                                new WorkbookColorInfo(colorList[7]),
                                null,
                                FillPatternStyle.Solid);

                            break;
                        }
                    }
                    
                    cellStartPos++;
                }

                rowStartPos += 2;
                cellStartPos = 4;
            }

            worksheet1.MergedCellsRegions.Add(0, cellsStart, 1, cellsStart);
            worksheet1.Columns[2].Width = 3000;
            worksheet1.Rows[0].Cells[cellsStart].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[0].Cells[cellsStart].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[0].Cells[cellsStart].CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
            worksheet1.Rows[0].Cells[cellsStart].Value = "DIAS";

            // Border cell = 
            worksheet1.Rows[0].Cells[cellsStart].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[cellsStart].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[0].Cells[cellsStart].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[cellsStart].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[0].Cells[cellsStart].CellFormat.TopBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[1].Cells[cellsStart].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;

            worksheet1.Columns[cellsCount + 1].Width = 5000;
            worksheet1.Rows[cellsStart / 2].Cells[cellsCount + 1].Value = "Estimados = " + sumEstimado;
            worksheet1.Rows[(cellsStart + 1) / 2].Cells[cellsCount + 1].Value = "Reales = " + sumReal;

            // Border cell =
            worksheet1.Rows[cellsStart / 2].Cells[cellsCount + 1].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[cellsStart / 2].Cells[cellsCount + 1].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[cellsStart / 2].Cells[cellsCount + 1].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[(cellsStart + 1) / 2].Cells[cellsCount + 1].CellFormat.RightBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[(cellsStart + 1) / 2].Cells[cellsCount + 1].CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin;
            worksheet1.Rows[(cellsStart + 1) / 2].Cells[cellsCount + 1].CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;

            // fourth part of the excel:
            for (int i = 2, x = 0; i < cellsStart; i+=2, x++)
            {
                if (tasks.Count == x) break;

                // dateSubstraction = tasks[x].InitialDate.Subtract(tasks[x].FinalDate);
                estimate = DateHandler.GetDateRangeEstimate(tasks[x]);
                real = DateHandler.GetDateRangeReal(tasks[x]);

                worksheet1.Rows[i].Cells[cellsStart].CellFormat.Alignment = HorizontalCellAlignment.Center;
                worksheet1.Rows[i].Cells[cellsStart].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                worksheet1.Rows[i].Cells[cellsStart].Value = estimate.Count;
                worksheet1.Rows[i + 1].Cells[cellsStart].CellFormat.Alignment = HorizontalCellAlignment.Center;
                worksheet1.Rows[i + 1].Cells[cellsStart].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                worksheet1.Rows[i + 1].Cells[cellsStart].Value = real.Count;
            }


            // Description of each color day:
            cellsCount /= 2;
            cellsCount += 2;
            int auditorCount = cellsCount;

            worksheet1.Columns[0].Width = 1200;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.Fill = new CellFillPattern(
                                new WorkbookColorInfo(colorList[5]), 
                                null, 
                                FillPatternStyle.Solid);

            worksheet1.Columns[1].Width = 5000;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].Value = "Sabados y domingos";

            cellsCount += 1;

            worksheet1.Columns[0].Width = 1200;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.Fill = new CellFillPattern(
                                new WorkbookColorInfo(colorList[8]),
                                null,
                                FillPatternStyle.Solid);

            worksheet1.Columns[1].Width = 5000;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].Value = "Dias de asueto";

            cellsCount += 1;

            worksheet1.Columns[0].Width = 1200;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].Value = "T";

            worksheet1.Columns[1].Width = 5000;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].Value = "TIEMPO";

            cellsCount += 1;

            worksheet1.Columns[0].Width = 1200;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].Value = "E";

            worksheet1.Columns[1].Width = 5000;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].Value = "ESTIMADO";

            cellsCount += 1;

            worksheet1.Columns[0].Width = 1200;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].Value = "R";

            worksheet1.Columns[1].Width = 5000;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].Value = "REAL";

            cellsCount += 1;

            worksheet1.Columns[0].Width = 1200;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.Fill = new CellFillPattern(
                                new WorkbookColorInfo(colorList[6]),
                                null,
                                FillPatternStyle.Solid);

            worksheet1.Columns[1].Width = 5000;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].Value = "ESTIMADO";

            cellsCount += 1;

            worksheet1.Columns[0].Width = 1200;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[0].CellFormat.Fill = new CellFillPattern(
                                new WorkbookColorInfo(colorList[7]),
                                null,
                                FillPatternStyle.Solid);

            worksheet1.Columns[1].Width = 5000;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.Alignment = HorizontalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            worksheet1.Rows[cellsCount].Cells[1].Value = "REAL";


            // Auditor description:
            WorksheetMergedCellsRegion mergedRegion2 = worksheet1.MergedCellsRegions.Add(auditorCount, 9, auditorCount, 20);

            foreach (var auditor in auditorsList)
            {
                mergedRegion2.Value = auditor.Abreviacion + " : " + auditor.NombreAuditor;

                auditorCount++;
                mergedRegion2 = worksheet1.MergedCellsRegions.Add(auditorCount, 9, auditorCount, 20);
            }

            Gantt.Save("Gantt_Chart.xls");
        }
    }
}
