﻿using DataAccess.DTOs;
using DataAccess.Models;

namespace Business.Activity
{
    public class ActivityHandler
    {
        public static List<ActivityDto> GetAllActivities()
        {
            try
            {
                GanttChartContext ctx = new();
                List<VwActividadAuditor> activityList = ctx.VwActividadAuditors.ToList();
                ActivityDto activityDto = new();
                List<ActivityDto> result = new();
                int aux = 0;

                foreach (VwActividadAuditor activity in activityList)
                {
                    if (aux == activity.IdActividad) continue;

                    aux = activity.IdActividad;
                    activityDto.IdActividad = activity.IdActividad;
                    activityDto.IdAuditor = activity.IdAuditor;
                    activityDto.DescripcionActividad = activity.DescripcionActividad;

                    List<VwAuditor> auditors = ctx.VwAuditors
                        .Where(q => q.IdActividad == activity.IdActividad)
                        .ToList();

                    foreach (VwAuditor auditor in auditors)
                    {
                        activityDto.Auditores ??= new List<VwAuditor>();

                        activityDto.Auditores?.Add(auditor);
                    }

                    result.Add(activityDto);
                    activityDto = new();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<TblAuditor> GetAllAuditors()
        {
            try
            {
                GanttChartContext ctx = new();
                List<TblAuditor> auditors = ctx.TblAuditors.ToList();
               
                return auditors;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
