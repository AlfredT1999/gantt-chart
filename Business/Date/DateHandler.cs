﻿using DataAccess.DTOs;
using DataAccess.Models;
using System.Globalization;

namespace Business.DateHandler
{
    public class DateHandler
    {
        public static List<Date> GetMonthsWithDays()
        {
            Date date = new();
            List<Date> dateList = new();
            DateTime month = new();
            int days = 0;

            for (int i = 1; i < 13; i++)
            {
                month = new(DateTime.Now.Year, i, 1);// Get current month
                days = DateTime.DaysInMonth(DateTime.Now.Year, i);
                date.Day = days;
                date.Month = month.ToString("MMMM", CultureInfo.CreateSpecificCulture("es-MX")).ToUpper();
                dateList.Add(date);
                date = new();
            }

            return dateList;
        }

        public static List<int> GetWeekendDays(DateDto date)
        {
            List<int> weekendDays = new();
            List<int> days = TotalDays(date);

            for (int i = 0; i < days.Count; i++)
            {
                DateTime month = new(DateTime.Now.Year, date.NumberMonth, days[i]);
                string dateInput = month.ToString("yyyy-MM-dd"); 
                DateTime dtResult = DateTime.ParseExact(dateInput, "yyyy-MM-dd", CultureInfo.CreateSpecificCulture("es-MX"));
                DayOfWeek today = dtResult.DayOfWeek;

                if ((today == DayOfWeek.Saturday) || (today == DayOfWeek.Sunday))
                {
                    weekendDays.Add(dtResult.Day);
                }
            }

            return weekendDays;
        }

        public static List<int> TotalDays(DateDto dates)
        {
            List<int> days = new();

            foreach (var day in dates.Days!)
            {
                days.Add(day);
            }

            return days;
        }

        public static List<DateRange> GetDateRangeEstimate(TaskDto task)
        {
            DateRange dateRange = new();
            List<DateRange> result = new();

            for (var day = task.InitialDate; day.Date <= task.FinalDate; day = day.AddDays(1))
            {
                DateTime month = new(DateTime.Now.Year, day.Month, day.Day);

                dateRange.Day = day.Day;

                if ((day.DayOfWeek == DayOfWeek.Saturday) || (day.DayOfWeek == DayOfWeek.Sunday)) continue;

                result.Add(new DateRange
                {
                    Day = day.Day,
                    Month = month.ToString("MMMM", CultureInfo.CreateSpecificCulture("es-MX")).ToUpper(),
                    IdActividad = task.IdActividad
                });
            }

            return result;
        }

        public static List<DateRange> GetDateRangeReal(TaskDto task)
        {
            DateRange dateRange = new();
            List<DateRange> result = new();

            for (var day = task.InitialDate; day.Date <= task.ActualDate; day = day.AddDays(1))
            {
                DateTime month = new(DateTime.Now.Year, day.Month, day.Day);

                dateRange.Day = day.Day;

                if ((day.DayOfWeek == DayOfWeek.Saturday) || (day.DayOfWeek == DayOfWeek.Sunday)) continue;

                result.Add(new DateRange
                {
                    Day = day.Day,
                    Month = month.ToString("MMMM", CultureInfo.CreateSpecificCulture("es-MX")).ToUpper(),
                    IdActividad = task.IdActividad
                });
            }

            return result;
        }

        public static List<int> GetDayOffs(DateDto date)
        {
            GanttChartContext ctx = new();
            List<CatDiasFeriado> catDayOffs = ctx.CatDiasFeriados.ToList();
            List<int> dayOffs = new();

            for(int i = 0; i < date.Days?.Count; i++)
            {
                for (int j = 0; j < catDayOffs.Count; j++)
                {
                    if (date.Days[i] == catDayOffs[j].FechaFeriada.Day && date.NumberMonth == catDayOffs[j].FechaFeriada.Month)
                    {
                        dayOffs.Add(date.Days[i]);
                    }
                }
            }

            return dayOffs;
        }
    }
}
