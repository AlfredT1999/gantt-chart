﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class RelActividadAuditor
    {
        public int Id { get; set; }
        public int TblActividadId { get; set; }
        public int TblAuditorId { get; set; }
        public DateTime? Inclusion { get; set; }
    }
}
