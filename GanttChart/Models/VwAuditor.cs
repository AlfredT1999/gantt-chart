﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class VwAuditor
    {
        public int IdActividad { get; set; }
        public int IdAuditor { get; set; }
        public string NombreAuditor { get; set; } = null!;
        public string? Abreviacion { get; set; }
    }
}
