﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class CatDiasFeriado
    {
        public int Id { get; set; }
        public DateTime FechaFeriada { get; set; }
        public DateTime? Inclusion { get; set; }
    }
}
