﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class VwActividadAuditor
    {
        public int IdActividad { get; set; }
        public int IdAuditor { get; set; }
        public string DescripcionActividad { get; set; } = null!;
        public string NombreAuditor { get; set; } = null!;
        public string? Abreviacion { get; set; }
    }
}
