﻿namespace DataAccess.Models
{
    public class Date
    {
        public int Day { get; set; }
        public string? Month { get; set; }
    }
}
