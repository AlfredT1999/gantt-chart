﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class TblAuditor
    {
        public int Id { get; set; }
        public string NombreAuditor { get; set; } = null!;
        public DateTime? Inclusion { get; set; }
        public string? Abreviacion { get; set; }
    }
}
