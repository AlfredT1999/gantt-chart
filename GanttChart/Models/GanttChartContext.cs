﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataAccess.Models
{
    public partial class GanttChartContext : DbContext
    {
        public GanttChartContext()
        {
        }

        public GanttChartContext(DbContextOptions<GanttChartContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CatDiasFeriado> CatDiasFeriados { get; set; } = null!;
        public virtual DbSet<RelActividadAuditor> RelActividadAuditors { get; set; } = null!;
        public virtual DbSet<TblActividad> TblActividads { get; set; } = null!;
        public virtual DbSet<TblAuditor> TblAuditors { get; set; } = null!;
        public virtual DbSet<VwActividadAuditor> VwActividadAuditors { get; set; } = null!;
        public virtual DbSet<VwAuditor> VwAuditors { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=10.10.0.32\\MSSQLSERVER2017;user=juanma;password=123;database=GanttChart");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CatDiasFeriado>(entity =>
            {
                entity.ToTable("cat_dias_feriados");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FechaFeriada)
                    .HasColumnType("date")
                    .HasColumnName("fecha_feriada");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<RelActividadAuditor>(entity =>
            {
                entity.ToTable("rel_actividad_auditor");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");

                entity.Property(e => e.TblActividadId).HasColumnName("tbl_actividad_id");

                entity.Property(e => e.TblAuditorId).HasColumnName("tbl_auditor_id");
            });

            modelBuilder.Entity<TblActividad>(entity =>
            {
                entity.ToTable("tbl_actividad");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DescripcionActividad).HasColumnName("descripcion_actividad");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TblAuditor>(entity =>
            {
                entity.ToTable("tbl_auditor");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Abreviacion)
                    .HasMaxLength(50)
                    .HasColumnName("abreviacion");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NombreAuditor).HasColumnName("nombre_auditor");
            });

            modelBuilder.Entity<VwActividadAuditor>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_actividad_auditor");

                entity.Property(e => e.Abreviacion)
                    .HasMaxLength(50)
                    .HasColumnName("abreviacion");

                entity.Property(e => e.DescripcionActividad).HasColumnName("descripcion_actividad");

                entity.Property(e => e.IdActividad).HasColumnName("id_actividad");

                entity.Property(e => e.IdAuditor).HasColumnName("id_auditor");

                entity.Property(e => e.NombreAuditor).HasColumnName("nombre_auditor");
            });

            modelBuilder.Entity<VwAuditor>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_auditor");

                entity.Property(e => e.Abreviacion)
                    .HasMaxLength(50)
                    .HasColumnName("abreviacion");

                entity.Property(e => e.IdActividad).HasColumnName("id_actividad");

                entity.Property(e => e.IdAuditor).HasColumnName("id_auditor");

                entity.Property(e => e.NombreAuditor).HasColumnName("nombre_auditor");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
