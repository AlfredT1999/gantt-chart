﻿namespace DataAccess.Models
{
    public class DateRange
    {
        public int IdActividad { get; set; }
        public string? Month { get; set; }
        public int Day { get; set; }
    }
}
