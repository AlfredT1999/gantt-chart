﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class TblActividad
    {
        public int Id { get; set; }
        public string DescripcionActividad { get; set; } = null!;
        public DateTime? Inclusion { get; set; }
    }
}
