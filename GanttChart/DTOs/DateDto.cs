﻿namespace DataAccess.DTOs
{
    public class DateDto
    {
        public int NumberMonth { get; set; }
        public string? Month { get; set; }
        public List<int>? Days { get; set; }
    }
}
