﻿using DataAccess.Models;

namespace DataAccess.DTOs
{
    public class ActivityDto
    {
        public int IdActividad { get; set; }
        public int IdAuditor { get; set; }
        public string DescripcionActividad { get; set; } = null!;
        public string NombreAuditor { get; set; } = null!;
        public string? Abreviacion { get; set; }
        public List<VwAuditor>? Auditores { get; set; }
    }
}
