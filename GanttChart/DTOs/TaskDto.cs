﻿namespace DataAccess.DTOs
{
    public class TaskDto
    {
        public int IdActividad { get; set; }
        public DateTime InitialDate { get; set; }
        public DateTime FinalDate { get; set; }
        public DateTime ActualDate { get; set; }
        public int Deviation { get; set; }
        public int Remainder { get; set; }
    }
}
